[![Netlify Status](https://api.netlify.com/api/v1/badges/8c878361-60a7-4ac3-83ab-2fde3bc83de3/deploy-status)](https://app.netlify.com/sites/brilliant-sable-5ca6db/deploys)

This is smaller blog sample using directory and Markdown.

URL is here: https://brilliant-sable-5ca6db.netlify.app/

After clone this repository, you can run using follow command.
(node require)

1. `npm install`
1. `npm run dev`
1. you can see `localhost:3000`

how can you add your blog
--
1. Save your file named your-file.md under posts/
1. you can see localhost:3000/posts/[your-file]
1. Save your directory and file named [your-directory]/[your-file].md under posts/
1. you can see  localhost:3000/posts/[your-directory]/[your-file]

This site is made from <https://github.com/vercel/next-learn/tree/master/basics/dynamic-routes-starter>

And added this: Catch all routes: <https://nextjs.org/docs/routing/dynamic-routes#catch-all-routes>

